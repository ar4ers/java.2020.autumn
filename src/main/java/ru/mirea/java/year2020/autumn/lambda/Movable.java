package ru.mirea.java.year2020.autumn.lambda;


public interface Movable {
    void moveUp();

    void moveDown();

    void moveLeft();

    void moveRight();

}

