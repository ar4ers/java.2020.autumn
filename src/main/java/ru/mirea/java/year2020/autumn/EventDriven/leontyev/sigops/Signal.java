package ru.mirea.java.year2020.autumn.EventDriven.leontyev.sigops;

public enum Signal {
    SIGINT("SIGINT", 2),
    SIGTERM("SIGTERM", 15),
    SIGABRT("SIGABRT", 22);

    public final String name;
    public final int number;

    Signal(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public void send() {
        Signals.raise(number);
    }
}
