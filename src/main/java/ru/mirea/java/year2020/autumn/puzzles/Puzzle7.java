package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle7 {
    public static void main(String[] args) {
        var i = Integer.MIN_VALUE;

        while (i == -i && i != 0) {
            System.out.println("=)");
        }
        System.out.println("=(");
    }
}