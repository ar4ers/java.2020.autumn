package ru.mirea.java.year2020.autumn.puzzles;

@SuppressWarnings("ALL")
class Jeopardy {
    int foo = 42;
    static String PRIZE = "$64,000";

    public int getFoo() {
        System.out.println("Jeopardy.getFoo");
        return foo;
    }
}

@SuppressWarnings("ALL")
class DoubleJeopardy extends Jeopardy {
    int foo = 44;
    static String PRIZE = "2 cents";

    @Override
    public int getFoo() {
        System.out.println("DoubleJeopardy.getFoo");
        return super.getFoo();
    }

    public static void main(String[] args) {
        DoubleJeopardy doubleJeopardy = new DoubleJeopardy();
        Jeopardy jeopardy = doubleJeopardy;

        System.out.println("--- fields ---");

        System.out.println(jeopardy.foo);
        System.out.println(doubleJeopardy.foo);

        System.out.println("--- methods ---");

        System.out.println(jeopardy.getFoo());
        System.out.println(doubleJeopardy.getFoo());

        System.out.println("--- static ---");

        System.out.println(DoubleJeopardy.PRIZE);
    }
}