package ru.mirea.java.year2020.autumn.smaple;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionSample {
    private static final Logger log = LogManager.getLogger(ExceptionSample.class);

    public static void main(String[] args) {
        try {
            new ExceptionSample().geFile(new File(""));
        } catch (IOException e) {
            log.error("Error happened!", e);
        } catch (NullPointerException e) {
            log.error("NPE happened!", e);
        } finally {
            log.info("Will always be printed");
        }
    }

    public void geFile(File file) throws FileNotFoundException {
        new FileInputStream(file);
    }
}
