package ru.mirea.java.year2020.autumn.smaple;

public class Bank {
    private final Algebra algebra;

    public Bank(Algebra algebra) {
        this.algebra = algebra;
    }

    public void transferMoney(Account from, Account to, int amountToTransfer) {
        if (from.getAmount() < amountToTransfer)
            throw new IllegalStateException("Insufficient funds on account '" + from.getId() + "'");

        from.setAmount(algebra.subtract(from.getAmount(), amountToTransfer));
        to.setAmount(algebra.add(to.getAmount(), amountToTransfer));
    }
}
