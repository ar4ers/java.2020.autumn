package ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples;

public abstract class Filtration<Condition, Operand> {

    private Condition predicate;

    public Filtration(Condition predicate) {
        this.predicate = predicate;
    }

    public void setPredicate(Condition predicate) {
        this.predicate = predicate;
    }

    public Condition getPredicate() {
        return predicate;
    }

    public abstract Operand filter(Operand source);
}
