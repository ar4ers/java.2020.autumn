package ru.mirea.java.year2020.autumn.puzzles;

class Cache {
    static {
        initializeIfNecessary();
    }

    private static int sum;

    public static int getSum() {
        initializeIfNecessary();
        return sum;
    }

    private static boolean initialized = false;

    private static void initializeIfNecessary() {
        if (!initialized) {
            for (int i = 0; i < 100; i++) {
                sum += i;
            }
            initialized = true;
        }
    }
}

public class Puzzle24 {
    public static void main(String[] args) {
        System.out.println(Cache.getSum());
    }
}