package ru.mirea.java.year2020.autumn.smaple;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ReadFromFile {
    private static final Logger log = LogManager.getLogger(ReadFromFile.class);

    public static void main(String[] args) {
        var filePath = "samples/sample.txt";
        var resourceAsStream = ReadFromFile.class.getClassLoader().getResourceAsStream(filePath);
        if (resourceAsStream != null) {
            new BufferedReader(new InputStreamReader(resourceAsStream)).lines().forEach(log::info);
        } else {
            log.warn("File '" + filePath + "' not found!");
        }
    }
}
