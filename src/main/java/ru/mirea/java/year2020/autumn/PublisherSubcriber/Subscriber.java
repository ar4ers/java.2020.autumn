package ru.mirea.java.year2020.autumn.PublisherSubcriber;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Subscriber {
    private static final Logger log = LogManager.getLogger(Subscriber.class);
    int id;
    public Subscriber(int id) {
        this.id = id;
    }

    @OnMessage
    private void onMessage(Object event, Object foo) {
        log.info("Subscriber.onMessage. ID: " + id + ". Event " + event);
    }

    @OnMessage
    private void onMessageOneMore(Object event) {
        log.info("\nSubscriber.onMessageOneMore. ID: " + id + ". Event " + event);
    }
}