package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle18 {
    public static void main(String[] args) {
        try {
            System.out.println("Hello world");
            System.exit(0);
        } finally {
            System.out.println("Goodbye world");
        }
    }

    public static void mainWithHook(String[] args) {
        System.out.println("Hello world");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("Goodbye world")));
        System.exit(0);
    }
}