package ru.mirea.java.year2020.autumn.PublisherSubcriber;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Object> subscribers = new ArrayList<>();
        Subscriber subscriber1 = new Subscriber(1);
        Subscriber subscriber2 = new Subscriber(2);
        Subscriber subscriber3 = new Subscriber(3);
        Event event = new Event();

        Event.operation.subscribe("Channel", subscriber1);
        Event.operation.subscribe("Channel", subscriber2);
        Event.operation.subscribe("Channel1", event);

        Event.operation.unsubscribe("Channel", subscriber1);
        Event.operation.unsubscribe("Channel", subscriber3); //Testing for warning appearing

        Event.operation.publish("Channel", subscribers);

    }
}
