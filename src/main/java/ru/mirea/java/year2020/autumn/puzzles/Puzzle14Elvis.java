package ru.mirea.java.year2020.autumn.puzzles;

import java.util.Calendar;

public class Puzzle14Elvis {
    private final int beltSize;
    private static final int CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    public static final Puzzle14Elvis INSTANCE = new Puzzle14Elvis();

    private Puzzle14Elvis() {
        beltSize = CURRENT_YEAR - 1930;
    }

    public int beltSize() {
        return beltSize;
    }

    public static void main(String[] args) {
        System.out.println("Elvis wears a size " + INSTANCE.beltSize() + " belt.");
    }
}