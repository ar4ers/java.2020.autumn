package ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample;

public interface Printable {
    void print();
}
