package ru.mirea.java.year2020.autumn.lambda;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class Lambda extends JFrame {
    private final JButton button = new JButton("Start");

    public Lambda() {
        super("Java 8 Lambda Example");
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                System.out.println("Classic way of handling event using Anonymous class");
            }
        });
        button.addActionListener(e -> System.out.println("Java 8 way" + " to handle event using Lambda expression"));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 200);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new Lambda().setVisible(true);
        });


        new Book("Jane Austen", "Pride and prejudice", 1813) {
            @Override
            public String description() {
                return "I loved this book!";
            }
        };

        new Movable() {
            @Override
            public void moveUp() {
                System.out.println("It moves up!");
            }

            @Override
            public void moveDown() {
                System.out.println("It moves down!");
            }

            @Override
            public void moveLeft() {
                System.out.println("It moves left!");
            }

            @Override
            public void moveRight() {
                System.out.println("It moves right!");
            }
        };
        Book anotherexample = new Book("Jane Austen", "Pride and prejudice", 1813) {
            @Override
            public String description() {
                return null;
            }
        };
        List<Book> books = new ArrayList<Book>();
        books.add(new Book("Rowling", "Harry Potter", 1997) {
                      @Override
                      public String description() {
                          return null;
                      }
                  }
        );


        new Book("Jane Austen", "Pride and prejudice", 1813) {
            @Override
            public String description() {
                return "I loved this book!";
            }
        };


        new Movable() {
            @Override
            public void moveUp() {
                System.out.println("It moves up!");
            }

            @Override
            public void moveDown() {
                System.out.println("It moves down!");
            }

            @Override
            public void moveLeft() {
                System.out.println("It moves left!");
            }

            @Override
            public void moveRight() {
                System.out.println("It moves right!");
            }
        };
        Book newBook = new Book("Jane Austen", "Pride and prejudice", 1813) {
            @Override
            public String description() {
                return null;
            }
        };
        List<Book> papers = new ArrayList<Book>();
        books.add(new Book("Rowling", "Harry Potter", 1997) {
                      @Override
                      public String description() {
                          return null;
                      }
                  }
        );


     /*   Can`t use static variables in anonymous classes unless they`re constant
            new Book() {
            static final int x = 0;
            static int y = 0;


        }; */

        int count = 10;
        Book two = new Book("Brown", "New life", 1994) {
            @Override
            public String description() {
                return "This book is " + count + "/10";
            }
        };
                /* local variables must be final
                count = 8; */
    }
}




