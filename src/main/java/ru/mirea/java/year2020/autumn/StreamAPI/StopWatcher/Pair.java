package ru.mirea.java.year2020.autumn.StreamAPI.StopWatcher;

public class Pair<First, Second> {

    /*
     *  A Java analogue of C++ template structure pair.
     */

    public final First first;
    public final Second second;

    public Pair(First first, Second second) {
        this.first = first;
        this.second = second;
    }
}
