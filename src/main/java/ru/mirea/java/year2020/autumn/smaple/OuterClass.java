package ru.mirea.java.year2020.autumn.smaple;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OuterClass {
    private static final Logger log = LogManager.getLogger(OuterClass.class);

    private int foo = 42;

    public OuterClass(int foo) {
        this.foo = foo;
    }

    public int getFoo() {
        return foo;
    }

    class Nested {
        public void setFooToEleven() {
            foo = 11;
        }
    }

    static class NestedStatic {
//        public int getFoo() {
//            System.out.println(foo);
//        }
    }

    public static void main(String[] args) {
        var outerClass = new OuterClass(43);
        var nested = outerClass.new Nested();
        nested.setFooToEleven();

        log.info(outerClass.getFoo());

        var nestedStatic = new OuterClass.NestedStatic();
    }
}
