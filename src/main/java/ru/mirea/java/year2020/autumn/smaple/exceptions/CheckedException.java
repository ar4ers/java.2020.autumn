package ru.mirea.java.year2020.autumn.smaple.exceptions;

public class CheckedException extends Exception {
    public CheckedException(String message) {
        super(message);
    }
}
