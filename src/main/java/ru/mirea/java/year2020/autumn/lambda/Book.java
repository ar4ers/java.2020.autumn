package ru.mirea.java.year2020.autumn.lambda;

public abstract class Book {
    private String author;
    private String name;
    private int year;

    public Book(String a, String n, int y) {
        author = a;


    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public abstract String description();
}
