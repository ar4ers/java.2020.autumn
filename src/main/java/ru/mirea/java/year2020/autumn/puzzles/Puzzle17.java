package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle17 {
    public static final long GUEST_USER_ID = -1;
    private static final long USER_ID = getUserId();

    private static long getUserId() {
        try {
            return getUserIdFromEnvironment();
        } catch (IdUnavailableException e) {
            System.out.println("Logging in as guest");
            return GUEST_USER_ID;
        }
    }

    private static long getUserIdFromEnvironment()
            throws IdUnavailableException {
        throw new IdUnavailableException(); // Simulate an error
    }

    public static void main(String[] args) {
        System.out.println("User ID: " + USER_ID);
    }
}

class IdUnavailableException extends Exception {
    IdUnavailableException() {
    }
}