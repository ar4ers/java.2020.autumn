package ru.mirea.java.year2020.autumn.puzzles;

class Exception1 {
    public static void main(String[] args) {
//        try {
//            System.out.println("Hello world");
//        } catch (IOException e) {
//            System.out.println("I’ve never seen println fail!");
//        }
    }
}

class Exception2 {
    public static void main(String[] args) {
        try {
            // If you have nothing nice to say, say nothing
        } catch (Exception e) {
            System.out.println("This can’t happen");
        }
    }
}

interface ExType1 {
    void f() throws CloneNotSupportedException;
}

interface ExType2 {
    void f() throws InterruptedException;
}

interface ExType3 extends ExType1, ExType2 {
}

class Exception3 implements ExType3 {

    @Override
    public void f() {
        System.out.println("Hello world");
    }

    public static void main(String[] args) {
        ExType3 t3 = new Exception3();
        t3.f();
    }
}