package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle0 {
    public static void main(String[] args) {
        for (byte b = Byte.MIN_VALUE; b < Byte.MAX_VALUE; b++) {
            if (b == 0x90) {
                System.out.println("=)");
            }
        }
    }
}