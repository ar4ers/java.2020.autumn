package ru.mirea.java.year2020.autumn.puzzles;

class Dog13 {
    public static void bark() {
        System.out.print("woof ");
    }
}

class SilentDog extends Dog13 {
    public static void bark() {
    }
}

public class Puzzle13 {
    public static void main(String args[]) {
        Dog13 woofer = new Dog13();
        Dog13 nipper = new SilentDog();
        woofer.bark();
        nipper.bark();
    }
}