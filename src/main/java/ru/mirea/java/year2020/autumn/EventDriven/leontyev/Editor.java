package ru.mirea.java.year2020.autumn.EventDriven.leontyev;

import ru.mirea.java.year2020.autumn.EventDriven.leontyev.fio.FileManager;
import ru.mirea.java.year2020.autumn.EventDriven.leontyev.sigops.Signal;
import ru.mirea.java.year2020.autumn.EventDriven.leontyev.sigops.Signals;

import javax.swing.*;
import java.awt.event.WindowEvent;
import java.io.IOException;

public final class Editor extends JFrame {

    private final Terminal term;

    public Editor() {
        setBounds(100, 100, 1240, 460);
        setTitle("Tiny J editor");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        term = new Terminal();
        add(term);
        setVisible(true);
        setResizable(false);

        Signals.makeIgnore(Signal.SIGABRT);
        Signals.makeIgnore(Signal.SIGTERM);
        Signals.makeIgnore(Signal.SIGINT);
    }

    @Override
    protected final void processWindowEvent(WindowEvent e) {
        if (e.getID() == 201) closeCurrentFile();
        super.processWindowEvent(e);
    }

    public void openFile(String filename) {
        try {
            term.activateFileOverwrite(new FileManager(filename));
            term.inform("File opened: " + filename);
        } catch (IOException ioe) {
            term.inform(ioe.getMessage());
        }
    }

    public void closeCurrentFile() {
        term.deactivateFileOverwrite();
        term.print("");
        term.inform("closing...");
    }

    public static void main(String[] args) {
        var ed = new Editor();
        ed.openFile("file.txt");
    }
}
