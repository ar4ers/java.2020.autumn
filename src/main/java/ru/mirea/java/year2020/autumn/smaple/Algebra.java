package ru.mirea.java.year2020.autumn.smaple;

public class Algebra {

    public int add(int first, int second) {
        return first + second;
    }

    public int subtract(int first, int second) {
        return first - second;
    }

    public int multiply(int first, int second) {
        return first * second;
    }

    public int divide(int first, int second) {
        if (second == 0)
            throw new ArithmeticException("Can not divide by zero!");
        return first / second;
    }
}

