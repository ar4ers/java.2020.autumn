package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle20 {
    private Puzzle20 internalInstance = new Puzzle20();

    public Puzzle20() throws Exception {
        throw new Exception("Exception text");
    }

    public static void main(String[] args) {
        try {
            Puzzle20 b = new Puzzle20();
            System.out.println("Surprise!");
        } catch (Exception ex) {
            System.out.println("I told you so");
        }
//        catch (Error err) {
//            System.out.println("I told you so for error");
//        }
    }
}