package ru.mirea.java.year2020.autumn.Object;

import java.util.Objects;

public class Object {
    String colour;
    int cost;
    int thickness;

    Object(String colour, int cost, int thickness) {
        this.colour = colour;
        this.cost = cost;
        this.thickness = thickness;
    }

    /* @Override
     public String toString() {
         return "Pen{" +
                 "colour='" + colour + '\'' +
                 ", cost=" + cost +
                 ", thickness=" + thickness +
                 '}';
     }
 */
    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Object object = (Object) o;
        return cost == object.cost &&
                //thickness == pen.thickness &&
                Objects.equals(colour, object.colour);
    }
    //to make collision we need equals method which does not contain one or more fields for comparison
    //for example we do not compare field thickness

    @Override
    public int hashCode() {
        return Objects.hash(colour, cost, thickness);
    }
    /*
    @Override
    public int hashCode() {//to make collision we need HashCode which does not depend from object's fields
        return 4565;
    }
    */
}