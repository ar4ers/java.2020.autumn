package ru.mirea.java.year2020.autumn.smaple;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggingSamples {
    private static final Logger log = LogManager.getLogger(LoggingSamples.class);

    public static void main(String[] args) {
        log.info("main method started");

        String foo = "default";
        if (args.length == 0) {
            log.warn("args is empty! using default values");
        } else {
            foo = args[0];
        }

        log.debug("About to check for valid input");

        try {
            if (foo.equals("Restricted")) {
                throw new IllegalArgumentException("'Restricted' value is prohibited!");
            }
        } catch (IllegalArgumentException ex) {
            log.error("Error during processing", ex);
        }

        log.info("main method ended");
    }
}
