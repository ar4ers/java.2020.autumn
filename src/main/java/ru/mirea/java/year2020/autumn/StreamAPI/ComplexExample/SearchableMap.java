package ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample;

import ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.Messages.LongMessage;
import ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.Messages.Message;

import java.util.Map;
import java.util.regex.Pattern;

public class SearchableMap {

    private final Map<String, String> map;

    public SearchableMap(Map<String, String> map) {
        this.map = map;
    }


    public Printable find(String input) {

        var keyStream = map.keySet().stream();
        final Printable result;

        Pattern regex = Pattern.compile(input, Pattern.CASE_INSENSITIVE);

        if (keyStream.anyMatch(x -> x.equals(input))) {
            result = new Message(
                    "Successfully found.",
                    input + "\t\t" + map.get(input)
            );
        } else {

            keyStream = map.keySet().stream();
            var matches = new LongMessage(
                    "Some matches found."
            );
            keyStream.filter(key -> regex.matcher(key).find()).map(
                    matchedKey -> regex.matcher(matchedKey)
                            .replaceFirst(input.toUpperCase()) +
                            " [" + matchedKey + "]\t\t" + map.get(matchedKey)
            ).forEach(matches::addContentLine);

            result = (matches.wasExtended()) ? matches : new Message(
                    "Request error.", "Nothing relevant."
            );
        }

        return result;
    }
}