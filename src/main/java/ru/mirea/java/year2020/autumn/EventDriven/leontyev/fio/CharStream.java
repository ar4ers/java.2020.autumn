package ru.mirea.java.year2020.autumn.EventDriven.leontyev.fio;

public interface CharStream<R> {
    R emit(char[] buff);
}
