package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle4 {
    public static void main(String[] args) {
        var i = Double.POSITIVE_INFINITY;

        while (i == i + 1) {
            System.out.println("=)");
        }
        System.out.println("=(");
    }
}
