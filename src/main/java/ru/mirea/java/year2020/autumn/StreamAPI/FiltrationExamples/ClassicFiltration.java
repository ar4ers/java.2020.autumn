package ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples;

import java.util.List;
import java.util.function.IntPredicate;

public class ClassicFiltration extends Filtration<IntPredicate, List<Integer>> {

    public ClassicFiltration(IntPredicate predicate) {
        super(predicate);
    }

    @Override
    public List<Integer> filter(List<Integer> source) {

        var i = source.iterator();
        while (i.hasNext()) {
            if (!getPredicate().test(i.next())) {
                i.remove();
            }
        }

        return source;
    }
}