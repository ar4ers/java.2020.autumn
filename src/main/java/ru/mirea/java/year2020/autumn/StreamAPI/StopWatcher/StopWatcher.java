package ru.mirea.java.year2020.autumn.StreamAPI.StopWatcher;

import java.util.function.Supplier;

public class StopWatcher {

    private StopWatcher() {

    }

    public static <First> Pair<First, Long> test(Supplier<First> supplier) {

        var startTime = System.nanoTime();
        var methodResult = supplier.get();
        var finishTime = System.nanoTime();

        return new Pair<>(methodResult, finishTime - startTime);
    }
}