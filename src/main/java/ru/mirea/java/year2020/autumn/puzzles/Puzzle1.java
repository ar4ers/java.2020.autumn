package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle1 {
    public static void main(String[] args) {
        int i = Integer.MAX_VALUE;
        System.out.println(Integer.toString(i, 2));
        System.out.println(Integer.toString(Integer.MIN_VALUE, 2));
        System.out.println(Integer.toString(Integer.MIN_VALUE + 1, 2));
        System.out.println(Integer.toString(Integer.MIN_VALUE + 2_147_483_000, 2));
        System.out.println(Integer.toString(-1, 2));
        int j = 43;

        i = i + j;
        System.out.println("After add: " + i);
        j = i - j;
        i = i - j;

        System.out.println(i);
        System.out.println(j);

        // i = ...0101010
        // j = ...0101011
        // First XOR
        // i = ...0000001
        // Second XOR
        // j = ...0101010
        // Third XOR
        // i = ...0101011
    }
}
