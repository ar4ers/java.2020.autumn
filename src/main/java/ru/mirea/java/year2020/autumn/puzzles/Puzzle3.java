package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle3 {
    public static final int END = Integer.MAX_VALUE;
    public static final int START = END - 100;

    public static void main(String[] args) {
        int count = 0;
        for (int i = START; i < END; i++) {
            System.out.println(count);
            count++;
        }
        System.out.println(count);
    }
}
