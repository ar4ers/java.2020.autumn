package ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.Messages;

import ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.Printable;

import static java.lang.System.out;

public class Message implements Printable {

    private final String title;
    private final String content;

    public Message(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public void print() {
        out.println(title + "\n\t" + content);
    }
}
