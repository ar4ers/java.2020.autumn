package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle6 {
    public static void main(String[] args) {
        var i = new Integer(0);
        var j = new Integer(0);

        while (i <= j && j <= i && i != j) {
            System.out.println("=)");
        }
        System.out.println("=(");
    }
}