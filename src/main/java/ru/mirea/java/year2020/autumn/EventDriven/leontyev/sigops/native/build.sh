
case $1 in
    "-v" | "--verbose") set -v;;
    "-h" | "--help ")   echo "-v / --verbose    - for verbose procession"
                        echo "-h / --help       - to see this"
                        exit 0;;
    *)                  :;;
esac

# DESTINATION DIR
dest=`pwd`/../../../../../../../../../../resources/sigops

# CLEANUP
if [ -f "${dest}/*.*" ]
    then rm ${dest}/*.*
fi
if [ -f "./obj/*.*" ]
    then rm ./obj/*.*
fi

# LOW HANDLER
echo ===COMPILATION_MESSAGES======= > build_log.low.txt
gcc -c -Wall -I ./inc -o ./obj/dc_signal_low.o -march=x86-64 ru_mirea_java_year2020_autumn_EventDriven_leontyev_sigops_Signals.c 2>> build_log.low.txt
echo ===LINKING_MESSAGES=========== >> build_log.low.txt
echo '*nix' >> build_log.low.txt
gcc -shared -fPIC -o ${dest}/dc_signal_low.so -march=x86-64 ./obj/dc_signal_low.o 2>> build_log.low.txt
echo --------------------- >> build_log.low.txt
echo mac >> build_log.low.txt
gcc -shared -fPIC -o ${dest}/dc_signal_low.dylib -march=x86-64 ./obj/dc_signal_low.o 2>> build_log.low.txt
echo --------------------- >> build_log.low.txt
echo win >> build_log.low.txt
gcc -shared -fPIC -o ${dest}/dc_signal_low.dll -march=x86-64 ./obj/dc_signal_low.o 2>> build_log.low.txt
echo --------------------- >> build_log.low.txt
