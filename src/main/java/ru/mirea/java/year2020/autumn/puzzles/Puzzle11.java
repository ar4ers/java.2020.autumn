package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle11 {
    private Puzzle11(Object o) {
        System.out.println("Object");
    }

    private Puzzle11(double[] dArray) {
        System.out.println("double array");
    }

    public static void main(String[] args) {
        new Puzzle11(null);
    }
}