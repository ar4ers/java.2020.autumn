package ru.mirea.java.year2020.autumn.EventDriven.leontyev.fio;

import java.io.*;

/*
 * It is not a real FileManager - just a concept, an idea.
 */
public final class FileManager {

    private final File fileBeingProcessed;
    private char[] buffer;

    public static class FileAccessException extends FileNotFoundException {
        private FileAccessException(String msg) {
            super(msg);
        }
    }

    public FileManager(String filename) throws IOException {

        fileBeingProcessed = new File(filename);
        if (fileBeingProcessed.exists()) {
            if (fileBeingProcessed.isFile()) {
                if (fileBeingProcessed.canRead()) {

                    var in = new FileReader(fileBeingProcessed);
                    var fileLen = fileBeingProcessed.length();
                    if (fileLen < (long) Integer.MAX_VALUE) {
                        buffer = new char[(int) ((fileLen + (long) Integer.MAX_VALUE) / 2)];

                        if (in.read(buffer) != buffer.length) {
                            throw new IOException("File i/o exception: some characters are not read.");
                        }

                    } else {
                        in.close();
                        throw new FileAccessException(
                                filename +
                                        " contains than 2^31 symbols (about 2-4 GiB - depends on the encoding)."
                        );
                    }
                    in.close();

                } else {
                    throw new FileAccessException(
                            filename + " - unable to read."
                    );
                }

            } else {
                throw new FileAccessException(
                        filename + " is a directory."
                );
            }

        } else {
            throw new FileNotFoundException(
                    filename + " - file does not exist."
            );
        }
    }

    public void reloadFile() throws IOException {

        if (fileBeingProcessed.canRead()) {
            var in = new FileReader(fileBeingProcessed);
            var fileLen = fileBeingProcessed.length();
            if (fileLen < (long) Integer.MAX_VALUE) {
                if (fileLen > buffer.length) {
                    char[] tmp = new char[(int) ((fileLen + (long) Integer.MAX_VALUE) / 2)];
                    System.arraycopy(tmp, 0, buffer, 0, buffer.length);
                    buffer = tmp;
                }

                if (in.read(buffer) != buffer.length) {
                    throw new IOException("File i/o exception: some characters are not read.");
                }

            } else {
                in.close();
                throw new FileAccessException(
                        fileBeingProcessed.getAbsolutePath() +
                                " contains than 2^31 symbols (about 2-4 GiB - depends on the encoding)."
                );
            }
            in.close();

        } else {
            throw new FileAccessException(
                    fileBeingProcessed.getAbsolutePath() + " - unable to read."
            );
        }
    }

    public void rewriteFile() throws IOException {

        if (fileBeingProcessed.canWrite()) {
            try (var out = new FileWriter(fileBeingProcessed)) {
                out.write(buffer);
            }

        } else {
            throw new FileAccessException(
                    fileBeingProcessed.getAbsolutePath() +
                            " - unable to write to."
            );
        }
    }

    public <R> R extractData(CharStream<R> charStream) {
        return charStream.emit(buffer);
    }
}
