package ru.mirea.java.year2020.autumn.EventDriven.leontyev;

import ru.mirea.java.year2020.autumn.EventDriven.leontyev.fio.FileManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Terminal extends JPanel {

    private static final int mainWidth = 1000;
    private static final int mainHeight = 400;

    private static final int infoWidth = 200;
    private static final int infoHeight = 350;

    private final ActionListener defaultActionListener = pushed -> inform("No file opened.");
    private ActionListener saveActionListener;

    private final TextArea mainSpace;
    private final TextArea infoSpace;
    private final JButton saveButton;

    public Terminal() {

        setLayout(null);

        mainSpace = new TextArea("", mainWidth, mainHeight, 1);
        mainSpace.setBounds(
                10, 10, mainWidth, mainHeight
        );
        add(mainSpace);

        infoSpace = new TextArea();
        infoSpace.setBounds(
                mainWidth + 20, 10, infoWidth, infoHeight
        );
        infoSpace.setEditable(false);
        add(infoSpace);

        saveButton = new JButton("Save");
        saveButton.setBounds(
                mainWidth + 20, 370, infoWidth, 20
        );
        add(saveButton);

        saveButton.addActionListener(defaultActionListener);
    }

    private ActionListener updateSaveActionListener(FileManager manager) {
        return pushed -> {
            try {
                manager.rewriteFile();
            } catch (IOException ioe) {
                inform(ioe.getMessage());
            }
        };
    }

    public void activateFileOverwrite(FileManager manager) {
        saveButton.removeActionListener(defaultActionListener);
        saveActionListener = updateSaveActionListener(manager);
        saveButton.addActionListener(saveActionListener);
    }

    public void deactivateFileOverwrite() {
        saveButton.removeActionListener(saveActionListener);
        saveButton.addActionListener(defaultActionListener);
    }

    public void print(String text) {
        mainSpace.setText(text);
    }

    public void inform(String info) {
        infoSpace.setText(info);
    }
}
