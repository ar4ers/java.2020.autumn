package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle19 {
    public static void main(String[] args) {
        workHard();
        System.out.println("It’s nap time.");
    }

    private static void workHard() {
        try {
            workHard();
        } finally {
            workHard();
        }
    }
}