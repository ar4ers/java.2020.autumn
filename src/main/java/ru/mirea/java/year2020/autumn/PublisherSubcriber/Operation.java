package ru.mirea.java.year2020.autumn.PublisherSubcriber;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Operation extends Event {
    private static final Logger log = LogManager.getLogger(Operation.class);

    void subscribe(String channelName, Object subscriber) {
        if (!channels.containsKey(channelName)) {
            channels.put(channelName, new ArrayList<>());
        }

        var subscribers = channels.get(channelName);

        if (!subscribers.contains(subscriber)) {
            subscribers.add(subscriber);
            log.info(channelName + ": Operation.subscribe");  //Checking for correct void building
        }
    }

    void unsubscribe(String channelName, Object subscriber) {
        if (!channels.containsKey(channelName)) {
            throw new IllegalArgumentException();
        } else {
            if (channels.get(channelName).remove(subscriber)) {
                log.info(channelName + ": Operation.unsubscribe successful");
            } else {
                log.info(channelName + ": WARN Operation.unsubscribe element not found!");
            }
        }
    }

    void publish(String channelName, Object event) {
        List<Object> subscribers = channels.get(channelName);

        if (subscribers != null) {
            subscribers.forEach(subscriber -> deliverMessage(subscriber, event));
        } else {
            log.warn("No subscribers for channel " + channelName + ". Skipping broadcast");
        }
    }

    void deliverMessage(Object subscriber, Object event) {
        Arrays.stream(subscriber.getClass().getDeclaredMethods())
                .filter(method -> method.getAnnotation(OnMessage.class) != null)
                .forEach(method -> {
                    method.setAccessible(true);
                    if (!invokeMethods(subscriber, event, method)) {
                        log.warn("\n------------------\nWrong input arguments on method" +
                                method + "\n------------------ \n");
                    }
                });
    }

    Boolean invokeMethods(Object subscriber, Object event, Method method) {
        try {
            method.invoke(subscriber, event);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            return false;
        }
        return true;
    }
}
