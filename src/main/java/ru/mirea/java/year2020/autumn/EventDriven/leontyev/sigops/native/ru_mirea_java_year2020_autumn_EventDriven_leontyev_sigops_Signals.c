#include <signal.h>
#include "ru_mirea_java_year2020_autumn_EventDriven_leontyev_sigops_Signals.h"

#define JNI_DC_CALL(retType, metName) \
JNIEXPORT retType JNICALL Java_ru_mirea_java_year2020_autumn_EventDriven_leontyev_sigops_Signals_##metName

/*
 * Class:     ru_mirea_java_year2020_autumn_EventDriven_leontyev_sigops_Signals
 * Method:    raise
 * Signature: (I)V
 */
JNI_DC_CALL(void, raise)(JNIEnv* env, jclass cls, jint sig_number) {
    raise( sig_number );
}

/*
 * Class:     ru_mirea_java_year2020_autumn_EventDriven_leontyev_sigops_Signals
 * Method:    handle
 * Signature: (II)V
 */
JNI_DC_CALL(void, handle)(JNIEnv* env, jclass cls, jint sig_number, jint hand_number) {
    signal( sig_number, (__p_sig_fn_t)hand_number );
}