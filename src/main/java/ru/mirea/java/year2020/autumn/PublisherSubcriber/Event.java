package ru.mirea.java.year2020.autumn.PublisherSubcriber;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;

public class Event {
    private static final Logger log = LogManager.getLogger(Event.class);
    static Operation operation = new Operation();
    static HashMap<String, List<Object>> channels = new HashMap<>();

    @OnMessage
    public void annotationReflectTester(Object event) {
        log.info("\nIf you see this message, you've already learned reflection!\n");
    }
}