package ru.mirea.java.year2020.autumn.smaple.patterns.factory;

interface Product {
}

class ConcreteProductA implements Product {
}

class ConcreteProductB implements Product {
}

abstract class Creator<T extends Product> {
    public abstract T createProduct();
}

class ConcreteCreatorA extends Creator<ConcreteProductA> {
    @Override
    public ConcreteProductA createProduct() {
        return new ConcreteProductA();
    }
}

class ConcreteCreatorB extends Creator<ConcreteProductB> {
    @Override
    public ConcreteProductB createProduct() {
        return new ConcreteProductB();
    }
}

public class FactoryMethodExample {
    public static void main(String[] args) {
        // an array of creators
        ConcreteCreatorA concreteCreatorA = new ConcreteCreatorA();
        ConcreteProductA ConcreteProductA = concreteCreatorA.createProduct();


        Creator<?>[] creators = {concreteCreatorA, new ConcreteCreatorB()};
        // iterate over creators and create products
        for (Creator<?> creator : creators) {
            Product product = creator.createProduct();
            System.out.printf("Created {%s}\n", product.getClass());
        }
    }
}