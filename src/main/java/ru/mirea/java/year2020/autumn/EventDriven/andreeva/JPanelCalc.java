package ru.mirea.java.year2020.autumn.EventDriven.andreeva;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class JPanelCalc extends JPanel {

    String res = "", res1 = "";
    String op = "";

    public JPanelCalc() {
        setLayout(null);
        final TextField txt1 = new TextField();
        txt1.setBounds(10, 10, 265, 25);
        add(txt1);

        JButton b0 = new JButton("0");
        b0.setBounds(10, 220, 45, 50);
        add(b0);

        JButton b1 = new JButton("1");
        b1.setBounds(10, 160, 50, 50);
        add(b1);

        JButton b2 = new JButton("2");
        b2.setBounds(60, 160, 50, 50);
        add(b2);

        JButton b3 = new JButton("3");
        b3.setBounds(110, 160, 50, 50);
        add(b3);

        JButton b4 = new JButton("4");
        b4.setBounds(10, 100, 50, 50);
        add(b4);

        JButton b5 = new JButton("5");
        b5.setBounds(60, 100, 50, 50);
        add(b5);

        JButton b6 = new JButton("6");
        b6.setBounds(110, 100, 50, 50);
        add(b6);

        JButton b7 = new JButton("7");
        b7.setBounds(10, 40, 50, 50);
        add(b7);

        JButton b8 = new JButton("8");
        b8.setBounds(60, 40, 50, 50);
        add(b8);

        JButton b9 = new JButton("9");
        b9.setBounds(110, 40, 50, 50);
        add(b9);

        JButton bResult = new JButton("=");
        bResult.setBounds(145, 220, 45, 50);
        add(bResult);

        JButton bPlus = new JButton("+");
        bPlus.setBounds(190, 40, 65, 50);
        add(bPlus);

        JButton bMinus = new JButton("-");
        bMinus.setBounds(190, 100, 65, 50);
        add(bMinus);

        JButton bMulti = new JButton("*");
        bMulti.setBounds(190, 160, 65, 50);
        add(bMulti);

        JButton bDivision = new JButton("/");
        bDivision.setBounds(190, 220, 65, 50);
        add(bDivision);

        JButton bC = new JButton("C");
        bC.setBounds(55, 220, 45, 50);
        add(bC);

        JButton change = new JButton("+-");
        change.setBounds(100, 220, 45, 50);
        add(change);

        b0.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 0);
            res = txt1.getText();

        });
        b1.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 1);
            res = txt1.getText();
        });

        b2.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 2);
            res = txt1.getText();
        });

        b3.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 3);
            res = txt1.getText();
        });

        b4.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 4);
            res = txt1.getText();
        });

        b5.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 5);
            res = txt1.getText();
        });

        b6.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 6);
            res = txt1.getText();
        });

        b7.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 7);
            res = txt1.getText();
        });

        b8.addActionListener(arg1 -> {
            txt1.setText(txt1.getText() + 8);
            res = txt1.getText();
        });

        b9.addActionListener(arg1 -> {

            txt1.setText(txt1.getText() + 9);
            res = txt1.getText();

        });
        bResult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                int num = Integer.parseInt(res);
                int num1 = Integer.parseInt(res1);
                String s = op;
                MetodCalc mc = new MetodCalc();
                txt1.setText(mc.calc(num1, s, num));
            }
        });
        bResult.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                bResult.setBackground(Color.GREEN);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                bResult.setBackground(UIManager.getColor("b2.background"));
            }
        });
        bPlus.addActionListener(arg1 -> {
            txt1.setText("");
            op = "+";
            res1 = res;
            res = "";
        });

        bMinus.addActionListener(arg1 -> {
            txt1.setText("");
            op = "-";
            res1 = res;
            res = "";
        });

        bMulti.addActionListener(arg1 -> {
            txt1.setText("");
            op = "*";
            res1 = res;
            res = "";
        });

        bDivision.addActionListener(arg1 -> {
            txt1.setText("");
            op = "/";
            res1 = res;
            res = "";
        });
        bC.addActionListener(arg1 -> {
            txt1.setText("");
            res = "";
            res1 = "";
            op = "";
        });
        change.addActionListener(arg1 -> {
            if (Integer.parseInt(txt1.getText()) > 0) {
                res = "-" + txt1.getText();
            } else {
                res = txt1.getText().substring(1);
            }
            txt1.setText(res);
        });
    }

}


