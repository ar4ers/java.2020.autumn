package ru.mirea.java.year2020.autumn.EventDriven.andreeva;

public class MetodCalc {
        public String calc ( int n1, String op, int n2) {
            int result;
            switch (op) {
                case "+":
                    result = n1+n2;
                    break;
                case "-":
                    result = n1-n2;
                    break;
                case "*":
                    result = n1*n2;
                    break;
                case "/":
                    if (n2==0) {return "ERROR";}
                    else { result = n1/n2; }
                    break;
                default:
                    result = 0;
                    break;
            }
            return Integer.toString(result);
        }
    }

