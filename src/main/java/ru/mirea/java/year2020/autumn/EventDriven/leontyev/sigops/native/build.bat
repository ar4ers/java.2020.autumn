@echo off
@if "%1"=="-h" goto :help
@if "%1"=="--help" goto :help
@if "%1"=="-v" echo on
@if "%1"=="--verbose" echo on

@rem DESTINATION DIR
@set "dest=%cd%\..\..\..\..\..\..\..\..\..\..\resources\sigops"

@rem CLEANUP
if exist %dest%\*.* del /q %dest%\*.*
if exist .\obj\*.* del /q .\obj\*.*

rem LOW HANDLER
echo ===COMPILATION_MESSAGES======= > build_log.low.txt
gcc -c -Wall -I .\inc -o .\obj\dc_signal_low.o -march=x86-64 ru_mirea_java_year2020_autumn_EventDriven_leontyev_sigops_Signals.c 2>> build_log.low.txt
echo ===LINKING_MESSAGES=========== >> build_log.low.txt
echo *nix >> build_log.low.txt
gcc -shared -fPIC -o %dest%\dc_signal_low.so -march=x86-64 .\obj\dc_signal_low.o 2>> build_log.low.txt
echo --------------------- >> build_log.low.txt
echo mac >> build_log.low.txt
gcc -shared -fPIC -o %dest%\dc_signal_low.dylib -march=x86-64 .\obj\dc_signal_low.o 2>> build_log.low.txt
echo --------------------- >> build_log.low.txt
echo win >> build_log.low.txt
gcc -shared -fPIC -o %dest%\dc_signal_low.dll -march=x86-64 .\obj\dc_signal_low.o 2>> build_log.low.txt
echo --------------------- >> build_log.low.txt

@goto :EOF

:help
@echo -v / --verbose    - for verbose procession
@echo -h / --help to    - to see this