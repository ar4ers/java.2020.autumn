package ru.mirea.java.year2020.autumn.smaple;

public class Account {
    private int amount = 0;
    private final String id;

    public Account(String id, int amount) {
        this.amount = amount;
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }
}
