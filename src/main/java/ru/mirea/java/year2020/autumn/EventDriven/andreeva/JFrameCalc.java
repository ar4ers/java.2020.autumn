package ru.mirea.java.year2020.autumn.EventDriven.andreeva;
import javax.swing.*;

public class JFrameCalc extends  JFrame {
        public JFrameCalc()
        {
            setBounds(100, 100, 280, 330);
            setTitle("Calculator");
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            add( new JPanelCalc());
            setVisible(true);
            setResizable(false);
        }

    public static void main(String[] args) {
        new JFrameCalc();
    }
}
