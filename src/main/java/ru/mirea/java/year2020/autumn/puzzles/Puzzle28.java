package ru.mirea.java.year2020.autumn.puzzles;

import java.util.HashSet;
import java.util.Set;

public class Puzzle28 {
    private final String first, last;

    public Puzzle28(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Puzzle28 n) {
        return n.first.equals(first) && n.last.equals(last);
    }

    public int hashCode() {
        return 31 * first.hashCode() + last.hashCode();
    }

    public static void main(String[] args) {
        Set<Puzzle28> s = new HashSet<Puzzle28>();
        s.add(new Puzzle28("Donald", "Duck"));
        System.out.println(s.contains(new Puzzle28("Donald", "Duck")));
    }
}