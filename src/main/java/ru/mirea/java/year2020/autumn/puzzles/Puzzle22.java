package ru.mirea.java.year2020.autumn.puzzles;

import java.util.HashMap;
import java.util.Map;

public class Puzzle22 {
    private Map<String, String> m = new HashMap<String, String>();

    public void MoreNames() {
        m.put("Mickey", "Mouse");
        m.put("Mickey", "Mantle");
    }

    public int size() {
        return m.size();
    }

    public static void main(String args[]) {
        Puzzle22 moreNames = new Puzzle22();
        System.out.println(moreNames.size());
    }
}