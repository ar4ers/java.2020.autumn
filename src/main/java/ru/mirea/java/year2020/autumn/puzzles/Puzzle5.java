package ru.mirea.java.year2020.autumn.puzzles;

public class Puzzle5 {
    public static void main(String[] args) {
        var i = Double.NaN;

        while (i != i) {
            System.out.println("=)");
        }
        System.out.println("=(");
    }
}
