package ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples;

import java.util.List;
import java.util.function.IntPredicate;

/*
 *  I've just found the way to shorten the filtration method even more!
 *  Moreover, it can improve our program's performance!
 *  It is the 'removeIf' collections' method. It also uses lambdas. =)
 */
public class RemoveIfFiltration extends Filtration<IntPredicate, List<Integer>> {

    public RemoveIfFiltration(IntPredicate predicate) {
        super(predicate);
    }

    @Override
    public List<Integer> filter(List<Integer> source) {
        source.removeIf(x -> !getPredicate().test(x));
        return source;
    }
}
