package ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples;

import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

public class ParallelFiltration extends Filtration<IntPredicate, List<Integer>> {

    public ParallelFiltration(IntPredicate predicate) {
        super(predicate);
    }

    @Override
    public List<Integer> filter(List<Integer> source) {
        return source.parallelStream()
                .filter(x -> getPredicate().test(x))
                .collect(Collectors.toList());
    }
}
