package ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.Messages;

import ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.Printable;

import java.util.LinkedList;
import java.util.List;

import static java.lang.System.out;

public class LongMessage implements Printable {

    private final String title;
    private final List<String> content;

    public LongMessage(String title) {
        this.title = title;
        content = new LinkedList<>();
    }

    public void addContentLine(String newLine) {
        content.add("\n\t" + newLine);
    }

    public boolean wasExtended() {
        return !content.isEmpty();
    }

    @Override
    public void print() {
        var tmp = content.toString();
        out.println(
                title + tmp.substring(1, tmp.lastIndexOf(']'))
        );
    }
}
