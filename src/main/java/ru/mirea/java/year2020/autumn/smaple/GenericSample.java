package ru.mirea.java.year2020.autumn.smaple;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GenericSample<VAR_TYPE_ONE extends Algebra, VAR_TYPE_TWO extends Frame> {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        stringList.add("Foo");
        stringList.add("Bar");
//        stringList.add(1);

        List<Integer> integerList = new ArrayList<>();
        integerList.add(42);
//        integerList.add("Foo");

        List rawList = integerList;
        rawList.add("Foo");

        System.out.println(integerList.get(0));
        System.out.println(rawList.get(0));

        System.out.println("----");

        System.out.println(integerList.get(1));
        System.out.println(rawList.get(1));

        System.out.println("----");
        System.out.println("42" + 42);
//        System.out.println(integerList.get(1) + 42); //  runtime exception
        System.out.println(rawList.get(1) + "42");
//        System.out.println(rawList.get(1) + 42);

        System.out.println("----");

        Number[] intArray = new Integer[4];
//        processArray(intArray);
//        processArray(new Integer[42]);
//        processArray(new Double[42]);
//        processArray(new String[42]);

//        intArray[0] = 42;
//        intArray[1] = 43L;
//        intArray[2] = 44D;
//        intArray[3] = 45F;
//        processArray(intArray);

        Arrays.stream(intArray).forEach(System.out::println);

//        List<Number>[] lists = new ArrayList<Number>[10];
        List<?>[] lists = new ArrayList<?>[10];
        List[] rawLists = new ArrayList[10];

        List<?> i = new ArrayList<Integer>();
//        i.add(42);
//        i.add(new Integer(52));
//        i.add(new Integer[4]);
//        Integer o = i.get(0);
//        processList(new ArrayList<Integer>());
//        processList(new ArrayList<String>());
//        processList(new ArrayList<ArrayList<ArrayList<String>>>());

//        List<Number> invariance = new ArrayList<Integer>();
        List<? extends Number> covariance = new ArrayList<Integer>();
        processExtendsList(covariance);
        List<? extends Number> covarianceFloat = new ArrayList<Float>();
        processExtendsList(covarianceFloat);
        List<? extends Number> covarianceDouble = new ArrayList<Double>();
        processExtendsList(covarianceDouble);
        covariance.add(null);

//        covariance.add(new Integer(42));
//        covariance.add(new Double(42D));
//        Number num = null;
//        covariance.add(num);
//        covariance.add(new Object());

        Number foo = covariance.get(0);
        Object foo1 = covariance.get(0);


        List<? super Integer> contravariance = new ArrayList<Number>();
        List<? super Integer> contravarianceInteger = new ArrayList<Integer>();
        List<? super Integer> contravarianceObject = new ArrayList<Object>();
//        contravariance.add(null);
        contravariance.add(new Integer(42));

//        contravariance.add(new Double(42D));
//        Number num = null;
//        contravariance.add(num);
//        contravariance.add(new Object());

        Object bar = contravariance.get(0);

        System.out.println("printing ---");
        // PECS
        // Producer Extends - Consumer Supers
        List<Integer> integers = new ArrayList<>();
        integers.add(43);
        integers.add(44);
        integers.add(45);
        integers.stream().map(new Function<Integer, String>() {
            @Override
            public String apply(Integer integer) {
                return "This is integer: " + integer.toString();
            }
        }).forEach(new Consumer<String>() {
            @Override
            public void accept(String x) {
                System.out.println(x);
            }
        });

        System.out.println("----");

        integers.forEach(System.out::println);

        System.out.println("----");

        List<String> typedList = GenericSample.<String>getTypedList();

        List<SimpleDtoExtended> dtos = new ArrayList<>();
        dtos.add(new SimpleDtoExtended("foo"));
        dtos.add(new SimpleDtoExtended("foo1"));
        dtos.add(new SimpleDtoExtended("foo2"));
        dtos.add(new SimpleDtoExtended("foo3"));

        dtos.stream().peek(new Consumer<SimpleDto>() {
            @Override
            public void accept(SimpleDto simpleDto) {
                System.out.println("Processing stream...");
                simpleDto.setName("new name");
            }
        }).toArray();

        dtos.forEach(System.out::println);

        System.out.println("----");

        var simpleDtoCollectionProcessor = new CollectionProcessor<SimpleDtoExtended>();

        Function<SimpleDto, String> mapper = new Function<>() {
            @Override
            public String apply(SimpleDto simpleDto) {
                return null;
            }
        };
        mapper.apply(new SimpleDtoExtended("gff"));

        var strings = simpleDtoCollectionProcessor.map(dtos, mapper);
        strings.forEach(s -> System.out.println("SimpleDto name is " + s));

        Function<Number, String> func = String::valueOf;

        List<Integer> list = List.of(1, 2);
        Stream<String> stream = list.stream().map(func);

        List<Long> listL = List.of(1L, 2L);
        Stream<String> streamL = list.stream().map(func);
    }

    static class CollectionProcessor<T> {

        <R> Collection<R> map(Collection<? extends T> source,
                              Function<? super T, R> mapper) {
            return source.stream().map(mapper).collect(Collectors.toList());
        }
    }

    static class ConsumerTest<T> {

        <R extends T> void accept(R t) {
            Object t1 = t;
        }
    }

    static class SimpleDto {
        String name;

        public SimpleDto(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "SimpleDto{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    static class SimpleDtoExtended extends SimpleDto {
        public SimpleDtoExtended(String name) {
            super(name);
        }
    }

    static <Res> List<Res> getTypedList() {
        return new ArrayList<Res>();
    }

    private static void processArray(Object[] numArray) {
        numArray[0] = 42;
        numArray[1] = 43L;
        numArray[2] = 44.3D;
        numArray[3] = 45.3F;
    }

    private static void processList(List<?> randomList) {
        if (randomList.get(1) instanceof Integer) {
            Integer o = (Integer) randomList.get(1);
        }
//        randomList.add(new Object());
    }

    private static void processExtendsList(List<? extends Number> numList) {
        numList.add(null);
//        numList.add(new Integer(42));
    }
}
