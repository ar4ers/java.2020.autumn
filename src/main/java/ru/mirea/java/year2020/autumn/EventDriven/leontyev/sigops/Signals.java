package ru.mirea.java.year2020.autumn.EventDriven.leontyev.sigops;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

/**
 * Complex substitute of
 * >   jdk.internal.misc.Signal,
 * >   sun.misc.Signal.
 */
public final class Signals {

    private Signals() {

    }

    public static final Consumer<Signal> HND_DFL;
    public static final Consumer<Signal> HND_IGN;
    public static final ConcurrentHashMap<Signal, Consumer<Signal>> handlers;

    static {

        HND_DFL = signal -> Signals.handle(signal.number, 0);
        HND_IGN = signal -> Signals.handle(signal.number, 1);

        handlers = new ConcurrentHashMap<>(3);

        String libName = "sigops/dc_signal_low.";
        switch (System.getProperty("sun.desktop")) {
            case "windows":
                libName += "dll";
                break;

            case "linux":
                libName += "so";
                break;

            case "mac":
                libName += "dylib";
                break;

            default:
                System.err.println(
                        "Unknown platform. Unable to load native signal lib."
                );
                System.exit(-1);
        }

        try {
            System.load(
                    new File(
                            Objects.requireNonNull(
                                    Signal.class.getClassLoader().getResource(libName)
                            ).toURI()
                    ).getAbsolutePath()
            );
        } catch (URISyntaxException | NullPointerException e) {
            System.err.println("Missing native lib file: " + libName);
            System.exit(-2);
        }
    }

    public static void makeIgnore(Signal signal) {
        if (!handlers.containsKey(signal)) {
            handlers.put(signal, HND_IGN);
            Signals.handle(signal.number, 1);
        }
    }

    public static void reset(Signal signal) {
        if (handlers.containsKey(signal)) {
            handlers.remove(signal);
            Signals.handle(signal.number, 0);
        }
    }

    static native void raise(int number);

    static native void handle(int sigNumber, int handNumber);
}
