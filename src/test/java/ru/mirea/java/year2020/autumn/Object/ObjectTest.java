package ru.mirea.java.year2020.autumn.Object;

import org.junit.jupiter.api.Test;

class ObjectTest {
    @Test
    public void checkHasCode() {
        Object object1 = new Object("Black", 20, 2);
        System.out.println("test 1   " + object1.hashCode());
        System.out.println(object1);
    }

    @Test
    public void checkToString() {
        Object object1 = new Object("Black", 20, 1);
        System.out.println(object1);
        //assertEquals("Pen{colour='Black', cost=20, thickness=1}",pen1.toString());
    }

    @Test
    public void checkEquals() {
        Object object1 = new Object("Black", 20, 8);
        Object object2 = new Object("Black", 20, 2);
        System.out.println(object1.hashCode());
        System.out.println(object2.hashCode());
        System.out.println(object1.hashCode() == object2.hashCode());
        System.out.println(object1.equals(object2));
    }
}
