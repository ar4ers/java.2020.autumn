package ru.mirea.java.year2020.autumn.StreamAPI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import ru.mirea.java.year2020.autumn.StreamAPI.ComplexExample.SearchableMap;
import ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples.ClassicFiltration;
import ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples.ParallelFiltration;
import ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples.RemoveIfFiltration;
import ru.mirea.java.year2020.autumn.StreamAPI.FiltrationExamples.StreamFiltration;
import ru.mirea.java.year2020.autumn.StreamAPI.StopWatcher.StopWatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;


class Task012Test {

    private static final Logger log = LogManager.getLogger(Task012Test.class);

    private void filTestPrint(String msg, List<Integer> source) {
        log.info(msg);
        source.stream().limit(10).forEach(val -> System.out.print(val + " "));
        System.out.println('\n');
    }

    private void mapTestPrint(String input, SearchableMap map) {
        log.info("Input:\t" + input);
        map.find(input).print();
        System.out.println('\n');
    }

    @Test
    void filtrationTest() {

        log.info("\nFiltration test.");

        IntPredicate predicate = x -> (x > 0 && x < Integer.MAX_VALUE / 2);
        var classicFiltration = new ClassicFiltration(predicate);
        var removeIfFiltration = new RemoveIfFiltration(predicate);
        var streamFiltration = new StreamFiltration(predicate);
        var parallelFiltration = new ParallelFiltration(predicate);

        var sourceA = new Random().ints(4096)
                .boxed().collect(Collectors.toList());
        var sourceB = new ArrayList<>(sourceA);
        var sourceC = new ArrayList<>(sourceA);
        var sourceD = new ArrayList<>(sourceA);

        filTestPrint("The source sequence (10 first):", sourceA);

        var pair =
                StopWatcher.test(() -> classicFiltration.filter(sourceA));
        filTestPrint("Classic:\t" + pair.second, pair.first);
        /*
         *  Format:
         *      >   [filtration_name]   [time_of]
         *      >   [   ...members_of_list...   ]
         */


        pair = StopWatcher.test(() -> removeIfFiltration.filter(sourceB));
        filTestPrint("RemoveIf:\t" + pair.second, pair.first);


        pair = StopWatcher.test(() -> streamFiltration.filter(sourceC));
        filTestPrint("Stream:\t\t" + pair.second, pair.first);


        pair = StopWatcher.test(() -> parallelFiltration.filter(sourceD));
        filTestPrint("Parallel:\t" + pair.second, pair.first);
    }

    @Test
    void searchableMapTest() {

        log.info("\nSearchableMap test.");

        var hashMap = new HashMap<String, String>();
        hashMap.put("Something", "Its description");
        hashMap.put("Another", "Its description");
        log.info("Map" + hashMap + "\n");

        var searchableHashMap = new SearchableMap(hashMap);

        mapTestPrint("something", searchableHashMap);
        mapTestPrint("Another", searchableHashMap);
        mapTestPrint("th", searchableHashMap);
        mapTestPrint("let", searchableHashMap);
    }
}