package ru.mirea.java.year2020.autumn.smaple;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AlgebraTest {

    private Algebra algebra = new Algebra();

    @Test
    void add() {
        assertEquals(42, algebra.add(40, 2));
    }

    @Test
    void multiply() {
    }

    @Test
    void divide() {
        assertEquals(3, algebra.divide(6, 2));
    }

    @Test
    void divideByZeroLambda() {
        assertThrows(ArithmeticException.class, () -> {
            algebra.divide(1, 0);
        }, "Can not divide by zero!");
    }

    @Test
    void divideByZeroAnonymous() {
        assertThrows(ArithmeticException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                algebra.divide(1, 0);
            }
        }, "Can not divide by zero!");
    }
}