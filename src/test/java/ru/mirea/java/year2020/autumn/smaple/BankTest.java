package ru.mirea.java.year2020.autumn.smaple;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BankTest {
    private final Algebra algebra = mock(Algebra.class);
    private final Bank bank = new Bank(algebra);

    @Test
    void transferMoney() {
        var from = new Account("id42", 42);
        var to = new Account("id43", 0);
        when(algebra.subtract(42, 5)).thenReturn(38);
        when(algebra.add(0, 5)).thenReturn(5);

        bank.transferMoney(from, to, 5);

        Assertions.assertEquals(38, from.getAmount());
        Assertions.assertEquals(5, to.getAmount());
    }
}